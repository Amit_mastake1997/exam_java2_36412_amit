<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%--to enable form binding : import spring supplied form tag lib --%>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<form:form method="post" modelAttribute="vendor">
		<table style="background-color: cyan; margin: auto;" border="1">
			<tr>
				<td>Enter Teachers Email</td>
				<td><form:input type="email" path="email" /></td>
				<td><form:errors path="email"/></td>
			</tr>
			<tr>
				<td>Enter Teachers Name</td>
				<td><form:input  path="name" /></td>
				<td><form:errors path="name"/></td>
			</tr>
			<tr>
				<td>Enter Password</td>
				<td><form:password  path="password" /></td>
				<td><form:errors path="password"/></td>
			</tr>
			<tr>
				<td>user_role</td>
				<td><form:input type="String" path="user_role" /></td>
				<td><form:errors path="user_role"/></td>
			</tr>
			<tr>
				<td>Choose date of birth</td>
				<td><form:input type="date" path="teacherDob" /></td>
				<td><form:errors path="teacherDob"/></td>
			</tr>

			<tr>
				<td><input type="submit" value="Register New Teachers" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>