package com.app.dao;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.pojos.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
	Optional<Teacher> findByName(String productName);
	//list all products containing supplied keyword in desc
	List<Teacher> findByTeacherId(Integer teacherId);
	
}
