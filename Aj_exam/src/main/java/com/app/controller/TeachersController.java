package com.app.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.pojos.Teacher;
import com.app.service.IteacherService;

@RestController // @Controller cls level annotation + @ResponseBody : added on ret type of ALL
				// request handling methods : annotated via : @RequestMapping , @GetMapping ...
@RequestMapping("/products")
@CrossOrigin
public class TeachersController {
	// dependency : service layer i/f
	@Autowired
	private IteacherService teacherService;

	public TeachersController() {
		System.out.println("in ctor of " + getClass().getName());
	}

	// add a req handling method to return representation of list of available
	// products
	@GetMapping
	public ResponseEntity<?> fetchAllTeachers() {
		System.out.println("in fetch all Teachers");
		List<Teacher> teachers = teacherService.getAllteachers();
		// chk if empty
		if (teachers.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		// non empty list
		return new ResponseEntity<>(teachers, HttpStatus.OK);
	}

	// add a req handling method to return representation of selected product by id
	// OR in case of invalid id
	// SC 404
	@GetMapping("/{teacherId}")
	public ResponseEntity<?> getTeachersDetails(@PathVariable int teacherId) {
		System.out.println("in get product dtls " + teacherId);
		try {
			return ResponseEntity.ok(teacherService.getteachersDetails(teacherId));
		} catch (RuntimeException e) {
			System.out.println("err in controller " + e);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	// add new REST end point : to fetch product by name
	@GetMapping("/name/{tName}")
	public ResponseEntity<?> getTeachersDetailsByName(@PathVariable String tName) {
		System.out.println("in get prd by name " + tName);
		try {
			return ResponseEntity.ok(teacherService. getteachersDetailsByName(tName));
		} catch (RuntimeException e) {
			System.out.println("err in controller " + e);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	// add new REST end point : to add a product
	@PostMapping
	public ResponseEntity<?> addNewTeacher(@RequestBody Teacher p) {
		System.out.println("in add new product " + p);
		return ResponseEntity.ok(teacherService.addteachersDetails(p));
	}

	// add new REST end point : to update existing product
	@PutMapping
	public ResponseEntity<?> updateTeachersDetails(@RequestBody Teacher p) {
		System.out.println("in update product " + p);
		try {
			return ResponseEntity.ok(teacherService.updateteachersDetails(p));
		} catch (RuntimeException e) {
			System.out.println("err in controller " + e);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	//add REST end point to delete product details
	@DeleteMapping("/{teacherId}")
	public void deleteTeachersDetails(@PathVariable int teacherId) {
		System.out.println("in del product dtls " + teacherId);
		try {
			teacherService.deleteteachersDetails(teacherId);
			
		} catch (RuntimeException e) {
			System.out.println("err in controller " + e);
		}
		
	}

}
