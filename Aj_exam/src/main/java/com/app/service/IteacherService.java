package com.app.service;

import java.util.List;
import com.app.pojos.Teacher;

public interface IteacherService {
//add a method to List all Products
  List<Teacher> getAllteachers();
  //add a method to get specific product details by its id
  Teacher getteachersDetails(int productId);
  //Get product details  by supplied name
  Teacher getteachersDetailsByName(String pName);
  //add new product details
  Teacher addteachersDetails(Teacher p);//p : transient
//update product details
  Teacher updateteachersDetails(Teacher p);//p : detached
  //delete product details
  void deleteteachersDetails(int productId);
}
