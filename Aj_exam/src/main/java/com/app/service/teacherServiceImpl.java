package com.app.service;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.teacherNotFoundException;
import com.app.dao.TeacherRepository;
import com.app.pojos.Teacher;

@Service // mandatory
@Transactional // optional since it's by default already added on JpaRepository
public class teacherServiceImpl implements IteacherService {
	// dependency
	@Autowired
	private TeacherRepository teacherRepo;

	@Override
	public List<Teacher> getAllteachers() {
		// TODO Auto-generated method stub
		return teacherRepo.findAll();
	}

	@Override
	public Teacher getteachersDetails(int teacherId) {
		// invoke dao's method
		Optional<Teacher> optionalTeacher = teacherRepo.findById(teacherId);
		if (optionalTeacher.isPresent())
			return optionalTeacher.get();
		// if product is not found : throw custom exception
		throw new teacherNotFoundException("Teacher Not Found : Invalid ID " + teacherId);
	}

	@Override
	public Teacher getteachersDetailsByName(String name) {
		// invoke repo's method
		Optional<Teacher> optional = teacherRepo.findByName(name);
		if (optional.isPresent())
			return optional.get();
		// if product is not found : throw custom exception
		throw new teacherNotFoundException("teacher Not Found : Invalid Product Name " + name);

	}

	@Override
	public Teacher addteachersDetails(Teacher t) {
		// TODO Auto-generated method stub
		return teacherRepo.save(t);
	}// auto dirty chking --insert query --L1 cache destroyed --cn rets cn pool
		// --persistence ctx is closed
		// rets detached POJO to the caller(REST controller)

	@Override
	public Teacher updateteachersDetails(Teacher t) {
		// chk if product exists
		Optional<Teacher> optional = teacherRepo.findById(t.getTeacherId());
		if (optional.isPresent())
			return teacherRepo.save(t); // update
		// if product is not found : throw custom exception
		throw new teacherNotFoundException("teachers Not Found : Invalid Product id " + t.getTeacherId());

	}
	// auto dirty chking --update query --L1 cache destroyed --cn rets cn pool
	// --persistence ctx is closed
	// rets detached POJO to the caller(REST controller)

	@Override
	public void deleteteachersDetails(int teacherId) {
		// chk if product exists : yes : delete , otherwise throw exc.
		Optional<Teacher> optional = (Optional<Teacher>) teacherRepo.findById(teacherId);
		if (optional.isPresent())
			teacherRepo.deleteById(teacherId);
		else
			// if product is not found : throw custom exception
			throw new teacherNotFoundException("teachers Not Found : Invalid Product id " + teacherId);

	}

}
