package com.app.pojos;

/*
 * id: number;
    productName: string;
    price: Number;
    productDesc: string;
    expDate: any;
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
@Table(name = "teacher_tbl")
@JsonInclude(Include.NON_DEFAULT)
public class subject {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id")
	private Integer subjectId;
	@Column(length = 20)
	@JsonProperty("teacherName")
	private String s_name;
	@Column(length = 50)
	private Integer s_duration;
	@Column(length = 20)
	private String s_course;
	@Column(length = 20)
	private Integer teacherId;
	public subject(Integer subjectId, String s_name, Integer s_duration, String s_course, Integer teacherId) {
		super();
		this.subjectId = subjectId;
		this.s_name = s_name;
		this.s_duration = s_duration;
		this.s_course = s_course;
		this.teacherId = teacherId;
	}
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public String getS_name() {
		return s_name;
	}
	public void setS_name(String s_name) {
		this.s_name = s_name;
	}
	public Integer getS_duration() {
		return s_duration;
	}
	public void setS_duration(Integer s_duration) {
		this.s_duration = s_duration;
	}
	public String getS_course() {
		return s_course;
	}
	public void setS_course(String s_course) {
		this.s_course = s_course;
	}
	public Integer getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	@Override
	public String toString() {
		return "subject [subjectId=" + subjectId + ", s_name=" + s_name + ", s_duration=" + s_duration + ", s_course="
				+ s_course + ", teacherId=" + teacherId + "]";
	}
	
	
	
}
