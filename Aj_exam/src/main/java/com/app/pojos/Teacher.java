package com.app.pojos;

/*
 * id: number;
    productName: string;
    price: Number;
    productDesc: string;
    expDate: any;
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
@Table(name = "teacher_tbl")
@JsonInclude(Include.NON_DEFAULT)
public class Teacher {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id")
	private Integer teacherId;
	@Column(length = 20)
	@JsonProperty("teacherName")
	private String name;
	@Column(length = 50)
	private String teacherEmail;
	@Column(length = 20)
	private String teacherpass;
	@Column(length = 20)
	private String teacherDob;
	@Enumerated(EnumType.STRING)
	@Column(name="user_role",length = 20)
	private Role userRole;
	public Teacher() {
		// TODO Auto-generated constructor stub
	}
	public Teacher(Integer teacherId, String name, String teacherEmail, String teacherpass, String teacherDob,
			com.app.pojos.Role userRole) {
		super();
		this.teacherId = teacherId;
		this.name = name;
		this.teacherEmail = teacherEmail;
		this.teacherpass = teacherpass;
		this.teacherDob = teacherDob;
		this.userRole = userRole;
	}
	public Integer getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTeacherEmail() {
		return teacherEmail;
	}
	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}
	public String getTeacherpass() {
		return teacherpass;
	}
	public void setTeacherpass(String teacherpass) {
		this.teacherpass = teacherpass;
	}
	public String getTeacherDob() {
		return teacherDob;
	}
	public void setTeacherDob(String teacherDob) {
		this.teacherDob = teacherDob;
	}
	public Role getUserRole() {
		return userRole;
	}
	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}
	@Override
	public String toString() {
		return "Teacher [teacherId=" + teacherId + ", name=" + name + ", teacherEmail=" + teacherEmail
				+ ", teacherpass=" + teacherpass + ", teacherDob=" + teacherDob + ", userRole=" + userRole + "]";
	}
	
	
}
